@extends('layout.master')
@section('judul')
    Tambah Employee
@endsection
@section('content')
    
    <form action="/employee" method="POST">
    @csrf
    <div class="form form-group">
        <label for="username">Username</label>
        <input type="text" class="form-control" name="username" placeholder="Masukan username">
        @error('username')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form form-group">
        <label for="password">Password</label>
        <input type="password" class="form-control" name="password" placeholder="Masukan password">
        @error('password')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form form-group">
        <label for="name">Nama</label>
        <input type="name" class="form-control" name="name" placeholder="Masukan nama">
        @error('name')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form form-group">
        <label for="email">Email</label>
        <input type="email" class="form-control" name="email" placeholder="Masukan email">
        @error('email')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form form-group">
        <label for="alamat">Alamat</label>
        <input type="alamat" class="form-control" name="alamat" placeholder="Masukan alamat">
        @error('alamat')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form form-group">
        <label for="telp">Telepon</label>
        <input type="telp" class="form-control" name="telp" placeholder="Masukan telp">
        @error('telp')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form form-group">
        <label for="departement_id">Level</label>
        <select name="departement_id" class="form-control" id="">
            <option value="">--Pilih level--</option>
        </select>
        @error('password')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary" data-target=".tambah" >Tambah</button>
    </form>

@endsection