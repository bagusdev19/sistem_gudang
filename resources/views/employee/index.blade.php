@extends('layout.master')
@section('judul')
    Employee
@endsection
@section('content')
    <a href="employee/create" class="btn btn-primary mb-3">Tambah data</a>
    <table class="table">
        <thead>
            <tr>
                <th scope="col" class="col-1">No.</th>
                <th scope="col">Nama</th>
                <th scope="col">Email</th>
                <th scope="col">Alamat</th>
                <th scope="col">Telepon</th>
                <th scope="col">Level</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
           
            {{-- @forelse ($employee as $key=>$value) --}}
                <tr>
                    <td>
                        {{-- {{ ++$i }} --}}
                    </td>
                    <td>
                        {{-- {{ $value->nama }} --}}
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                        <form action="
                        {{-- /departement/{{ $value->id }} --}}
                        " method="POST">
                            @csrf
                            @method('DELETE')
                            <a href="departement/
                            {{-- {{ $value->id }} --}}
                            " class="btn btn-primary"><i class="far fa-eye"></i></a>
                            <a href="departement/
                            {{-- {{ $value->id }} --}}
                            /edit" class="btn btn-primary"><i class="fas fa-edit"></i></a>
                            <button class="btn btn-danger"><i class="far fa-trash-alt"></i></button>
                        </form>
                    </td>    
                </tr>
            {{-- @empty
            
            @endforelse --}}
        </tbody>
    </table>

@endsection