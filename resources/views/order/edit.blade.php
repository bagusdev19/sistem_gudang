@extends('layout.master')
@section('judul')
    Edit Order
@endsection
@section('content')
    
    <form action="/order" method="POST">
    @csrf
    <div class="form form-group">
        <label for="invoice">Invoice</label>
        <input type="text" class="form-control" name="invoice" value="">
        @error('invoice')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form form-group">
        <label for="tanggal">Tanggal</label>
        <input type="date" class="form-control" name="tanggal" value="">
        @error('tanggal')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form form-group">
        <label for="jumlah_barang">Jumlah Barang</label>
        <input type="number" class="form-control" name="jumlah_barang" value="">
        @error('jumlah_barang')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form form-group">
        <label for="total_harga">Total Harga</label>
        <input type="number" class="form-control" name="total_harga" value="">
        @error('total_harga')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form form-group">
        <label for="status">Status</label>
        <input type="text" class="form-control"disabled>
        </select>
        @error('status')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form form-group">
        <label for="customer_id">Id Customer</label>
        <input type="customer_id" class="form-control" name="customer_id" value="">
        @error('customer_id')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form form-group">
        <label for="user_id">Id User</label>
        <input type="user_id" class="form-control" name="user_id" value="">
        @error('user_id')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary" >Edit</button>
    </form>

@endsection