@extends('layout.master')
@section('judul')
    Tambah Order
@endsection
@section('content')
    
    <form action="/order" method="POST">
    @csrf
    <div class="form form-group">
        <label for="invoice">Invoice</label>
        <input type="text" class="form-control" name="invoice" placeholder="Masukan invoice">
        @error('invoice')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form form-group">
        <label for="tanggal">Tanggal</label>
        <input type="date" class="form-control" name="tanggal" placeholder="Masukan tanggal">
        @error('tanggal')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form form-group">
        <label for="jumlah_barang">Jumlah Barang</label>
        <input type="number" class="form-control" name="jumlah_barang" placeholder="Masukan Jumlah">
        @error('jumlah_barang')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form form-group">
        <label for="total_harga">Total Harga</label>
        <input type="number" class="form-control" name="total_harga" placeholder="Masukan Jumlah">
        @error('total_harga')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form form-group">
        <label for="status">Status</label>
        <input type="text" class="form-control"disabled>
        </select>
        @error('status')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form form-group">
        <label for="customer_id">Id Customer</label>
        <input type="customer_id" class="form-control" name="customer_id" placeholder="Masukan nama">
        @error('customer_id')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form form-group">
        <label for="user_id">Id User</label>
        <input type="user_id" class="form-control" name="user_id" placeholder="Masukan nama">
        @error('user_id')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary" >Tambah</button>
    </form>

@endsection