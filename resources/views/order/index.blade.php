@extends('layout.master')
@section('judul')
    Data Order
@endsection
@section('content')
    <a href="order/create" class="btn btn-primary mb-3">Tambah Barang</a>
    <table class="table">
        <thead>
            <tr>
                <th scope="col" class="col-1">No.</th>
                <th scope="col">Invoice</th>
                <th scope="col">Tanggal</th>
                <th scope="col">Jumlah Barang</th>
                <th scope="col">Total Harga</th>
                <th scope="col">Id Customer</th>
                <th scope="col">Id User</th>
                <th scope="col">Status</th>
                <th scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
           
            {{-- @forelse ($employee as $key=>$value) --}}
                <tr>
                    <td>
                        {{-- {{ ++$i }} --}}
                    </td>
                    <td>
                        {{-- {{ $value->nama }} --}}
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td>
                        <form action="
                        {{-- /departement/{{ $value->id }} --}}
                        " method="POST">
                            @csrf
                            @method('DELETE')
                            <a href="order/
                            {{-- {{ $value->id }} --}}
                            " class="btn btn-primary"><i class="far fa-eye"></i></a>
                            <a href="order/
                            {{-- {{ $value->id }} --}}
                            /edit" class="btn btn-primary"><i class="fas fa-edit"></i></a>
                            <button class="btn btn-danger"><i class="far fa-trash-alt"></i></button>
                        </form>
                    </td>    
                </tr>
            {{-- @empty
            
            @endforelse --}}
        </tbody>
    </table>

@endsection