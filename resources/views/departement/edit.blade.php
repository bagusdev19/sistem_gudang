@extends('layout.master')
@section('judul')
    Edit Departemen {{ $departement->nama }}
@endsection
@section('content')
    <form action="/departement/{{ $departement->id }}" method="POST">
        @csrf
        @method('PUT')
        <div class="form form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" name="nama" value="{{ $departement->nama }}">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
@endsection