@extends('layout.master')
@section('judul')
    Tambah Departement
@endsection
@section('content')
    
    <form action="/departement" method="POST">
    @csrf
    <div class="form form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control" name="nama" placeholder="Masukan Nama Jabatan">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary" data-target=".tambah" >Tambah</button>
    </form>

@endsection