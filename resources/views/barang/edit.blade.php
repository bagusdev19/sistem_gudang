@extends('layout.master')
@section('judul')
    Edit Barang
@endsection
@section('content')
    
    <form action="/barang" method="POST">
        @method('PUT')        
        @csrf
        <div class="form form-group">
            <label for="nama">Nama Barang</label>
            <input type="text" class="form-control" name="nama" value="" placeholder="Masukan nama">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form form-group">
            <label for="jenis">Jenis Barang</label>
            <select name="jenis" class="form-control">
                <option value="">--Pilih Jenis Barang--</option>
            </select>
            @error('jenis')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form form-group">
            <label for="harga">Harga</label>
            <input type="harga" class="form-control" name="harga" value="" placeholder="Masukan nama">
            @error('harga')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form form-group">
            <label for="stock">Stock</label>
            <input type="stock" class="form-control" name="stock" value="" placeholder="Masukan stock">
            @error('stock')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form form-group">
            <label for="deskripsi">Deskripsi</label>
            <textarea name="deskripsi" class="form-control" value="" id="" cols="30" rows="10"></textarea>
            @error('deskripsi')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form form-group">
            <label for="suplier">Supplier</label>
            <select name="suplier" class="form-control">
                <option value="">--Pilih Suplier--</option>
            </select>
            @error('suplier')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary" value="">Edit</button>
    </form>

@endsection