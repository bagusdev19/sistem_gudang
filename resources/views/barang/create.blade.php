@extends('layout.master')
@section('judul')
    Tambah Barang
@endsection
@section('content')
    
    <form action="/barang" method="POST">
    @csrf
    <div class="form form-group">
        <label for="nama">Nama Barang</label>
        <input type="text" class="form-control" name="nama" placeholder="Masukan nama">
        @error('nama')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form form-group">
        <label for="jenis">Jenis Barang</label>
        <select name="jenis" class="form-control">
            <option value="">--Pilih Jenis Barang--</option>
        </select>
        @error('jenis')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form form-group">
        <label for="harga">Harga</label>
        <input type="harga" class="form-control" name="harga" placeholder="Masukan nama">
        @error('harga')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form form-group">
        <label for="stock">Stock</label>
        <input type="stock" class="form-control" name="stock" placeholder="Masukan stock">
        @error('stock')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form form-group">
        <label for="deskripsi">Deskripsi</label>
        <textarea name="deskripsi" class="form-control" id="" cols="30" rows="10"></textarea>
        @error('deskripsi')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form form-group">
        <label for="suplier">Supplier</label>
        <select name="suplier" class="form-control">
            <option value="">--Pilih Suplier--</option>
        </select>
        @error('suplier')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary" >Tambah</button>
    </form>

@endsection